# Ansible Setup Utility for Linux Desktop/Server VMs I build

```bash
ansible-playbook playbook.yml --tags "blah, blah, etc."
```

"tags": ```desktop```, ```server```, ```sudoers```, ```tftpd```
