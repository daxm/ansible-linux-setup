#!/usr/bin/env bash

now=`date +"%Y%m%d %T"`
echo Running dist-upgrade.sh at ${now}

echo "Running APT UPDATE"
sudo apt -y update
echo
echo "Running APT DIST-UPGRADE"
sudo apt -y dist-upgrade
echo
echo "Running APT AUTOREMOVE"
sudo apt -y autoremove --purge
echo
echo "Running APT AUTOCLEAN"
sudo apt -y autoclean
echo
echo "Running SNAP REFRESH"
sudo snap refresh
